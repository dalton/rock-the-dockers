{ system ? builtins.currentSystem }:

let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs { inherit system; };

  callPackage = pkgs.lib.callPackageWith pkgs;

  lsdalton = callPackage ./pkgs/lsdalton {};

  dockerImage = pkg:
    pkgs.dockerTools.buildImage {
      name = "registry.gitlab.com/dalton/lsdalton-v${pkg.version}";
      created = "now";
      contents = [
        pkg
        pkgs.busybox
        pkgs.python3
      ];
      config = {
        Env = [
          "DALTON_TMPDIR=/scratch"
        ];
        Volumes = {
          "/scratch" = {};
        };
      };
    };

in
dockerImage lsdalton
