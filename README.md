# rock-the-dockers

Build minimally-sized Docker images to run LSDALTON using Nix and GitLab CI.

The code is compiled with OpenMP parallelization enabled and links against Intel MKL.

Run it with:
```
docker run --rm -v $PWD:$PWD -v <scratch>:/scratch -w $PWD -u $(id -u $USER):$(id -g $USER) registry.gitlab.com/dalton/rock-the-dockers/lsdalton-v2020.1 lsdalton <input.dal> <molecule.mol>
```
where `<scratch>` must point to an existing, writable folder that will be used for scratch files.


## Are you new to Docker?

We recommend to install Docker using the standard package manager on your
operating system.

If you see the error "Cannot connect to the Docker daemon", you need to enable
the daemon: https://unix.stackexchange.com/a/279785

You may then see a permission error and may need to add your user to the
`docker` group:

```
$ sudo usermod -a -G docker $USER
```

Listing all images you have:

```
$ docker images -a
```

Remove all images:

```
$ docker rmi $(docker images -a -q)
```
